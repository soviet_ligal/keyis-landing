<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define('FS_METHOD', 'direct');
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'keys');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root1234');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&sqcO`g=^{e@=3 /?>J!0AFn1|$FR`UtY&i.<p!,-u?c@?gwSwWo!o-q qFp3rT`');
define('SECURE_AUTH_KEY',  'Zv>@t7w4&7u5fpc^j9l]VI<{*2laeiQp`cI5De!!y[@_M5W JV[>l@^IbT0Xy]HT');
define('LOGGED_IN_KEY',    'U({.,Ki!5vH{Oj35Ek2QkKW6k@eD{a(&<w.IcD=Wz:iA^hFw X%7##;fo/H{KZMR');
define('NONCE_KEY',        'O>o a$Fiq``x`</km1}c&=d..~)=$!4B-q*~i$u?4g$qFdgG~}1lu4tCDZUN6{{c');
define('AUTH_SALT',        'WKR^C4Y:g~/Bdhz,Dpi_RKYz965H14C_ y@vq|54u<Wp$(]wi_V}e:|>6[I;I(r[');
define('SECURE_AUTH_SALT', '@z^j[@)+e!!;5%2n*7*!56A=_F=Nk3d8eTl:;/P~(BOo>G)_BB86G_6RNG|urI2H');
define('LOGGED_IN_SALT',   'ANTyS=!]~[&@!0[>ZOs[<0A><Ul<VYRf62C_1yH$j(lWT&M(`^U,vYC[W/ukm~4s');
define('NONCE_SALT',       ' 95BCC?wm7#n,6MFW&)@-)VKDd`E?}&P=:N=OIVl/mctDpNx]`;X2OEP9[r=MXvG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
